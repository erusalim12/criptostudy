﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MD5Console
{
    class Program
    {
        static bool finished = false;
        private static byte counter;
        static void Main(string[] args)
        {
            char choice_ed = ' ', choice_tf = ' ';
            counter = 0;
            do
            {
                Console.WriteLine("Введите текст: \n");
                string eingabe = Console.ReadLine();
                Console.WriteLine("\nРеализованная функция:\t");
                Console.WriteLine(MD5.hash(eingabe));
                Console.WriteLine("\nБиблиотечная функция MD5 \t");
                Console.WriteLine(MD5DotNet(eingabe));
                Console.WriteLine(" \n\n\n");
            } while (!finished);
        }

        
        private static string MD5DotNet(string input)
        {

            // step 1, calculate MD5 hash from input

            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();

            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);


            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {

                sb.Append(hash[i].ToString("X2"));

            }

            return sb.ToString();

        }
    }

}
