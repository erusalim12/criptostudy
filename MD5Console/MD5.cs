﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Runtime.Remoting.Messaging;
using System.Text;

namespace MD5Console
{
    static class MD5
    {
        private static UTF8Encoding encoding = new UTF8Encoding();

        //const UInt32 a0 = 0x67452301;   //1732584193
        //const UInt32 b0 = 0xEFCDAB89;   //4023233417
        //const UInt32 c0 = 0x98BADCFE;   //2562383102
        //const UInt32 d0 = 0x10325476;   //271733878

        const UInt32 a0 = 0x01234567;   //1732584193
        const UInt32 b0 = 0x89ABCDEF;   //4023233417
        const UInt32 c0 = 0xFEDCBA98;   //2562383102
        const UInt32 d0 = 0x76543210;   //271733878

        //// T[i] = [2^32 * abs(sin(i))] правильная формула

        private static UInt32[] KFunc()
        {
            uint[] mas = new uint[64];
            for (int i = 0; i < mas.Count(); i++)
            {
                mas[i] = (UInt32)(Math.Pow(2, 32) * Math.Abs(Math.Sin(i)));
            }
            return mas;
        }

        #region значения функции K
        static UInt32[] K1 = {
            0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
            0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
            0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
            0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
            0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
            0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
            0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
            0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
            0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
            0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
            0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
            0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
            0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
            0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
            0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
            0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391
        };
        #endregion

        static int[] s = {7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
                             5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
                             4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
                             6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21};

        public static string hash(string message) //реализация с BitArray
        {
            var K = KFunc();
            BitArray[][] messagePreparated = Preparation(message);

            UInt32 A0 = a0;
            UInt32 B0 = b0;
            UInt32 C0 = c0;
            UInt32 D0 = d0;

            for (int i = 0; i < messagePreparated.Length; i++) //Для каждого блока 512 бит
            {
                //if (i == 0 || i == 1 || i == 15 || i == 31)
                //{
                //    Console.WriteLine("A0 = {0}",A0);
                //    Console.WriteLine("B0 = {0}",B0);
                //    Console.WriteLine("C0 = {0}",C0);
                //    Console.WriteLine("D0 = {0}",D0);
                //}
                int j = 0;
                for (int k = 0; k < 4; k++) //4 раунда
                {
                    if (k == 3)
                    {
                        Console.WriteLine("До перестановки");
                        Console.WriteLine("значение A = {0}", ((a0 % Int32.MaxValue) * (A0 % Int32.MaxValue)).ToString("X"));
                        Console.WriteLine("значение B = {0}", ((b0 % Int32.MaxValue) * (B0 % Int32.MaxValue)).ToString("X"));
                        Console.WriteLine("значение C = {0}", ((c0 % Int32.MaxValue) * (C0 % Int32.MaxValue)).ToString("X"));
                        Console.WriteLine("значение D = {0}", ((d0 % Int32.MaxValue) * (D0 % Int32.MaxValue)).ToString("X"));
                     
                    }
                   
                    for (int m = 0; m < 16; m++) //16 циклов вычисления
                    {
                        j++;
                        if (j == 1 || j == 2 || j == 16 || j == 32)
                        {
                            Console.WriteLine("-------Начало {0} -й итерации ---------",j);

                            Console.WriteLine("Раунд {0}", j);
                            Console.WriteLine("A0 = {0} (16) = {1} (10)", A0.ToString("X"),A0);
                            Console.WriteLine("B0 = {0} (16) = {1} (10)", B0.ToString("X"),B0);
                            Console.WriteLine("C0 = {0} (16) = {1} (10)", C0.ToString("X"),C0);
                            Console.WriteLine("D0 = {0} (16) = {1} (10)", D0.ToString("X"),D0);

                        }

                        //------создание копий-------
                        UInt32 BackupA0 = A0;
                        UInt32 BackupB0 = B0;
                        UInt32 BackupC0 = C0;
                        UInt32 BackupD0 = D0;
                        //-----операция смещения------
                        A0 = BackupD0; //D => A
                        D0 = BackupC0; //C => D
                        C0 = BackupB0; //B => C
                        //---MD5 операция A B----
                        switch (k)
                        {
                            case 0:
                                //f1
                                B0 = (rotate((((((BackupA0 + f1(BackupB0, BackupC0, BackupD0))) + get_part_message(messagePreparated, i, m)) + K[m])), s[m]) + BackupB0);
                                break;
                            case 1:
                                //f2
                                B0 =(rotate((((((BackupA0 + f2(BackupB0, BackupC0, BackupD0))) + get_part_message(messagePreparated, i, m)) + K[m + 16])), s[m + 16]) + BackupB0);
                                break;
                            case 2:
                                //f3
                                B0 = (rotate((((((BackupA0 + f3(BackupB0, BackupC0, BackupD0))) + get_part_message(messagePreparated, i, m)) + K[m + 32])), s[m + 32]) + BackupB0);
                                break;
                            case 3:
                                //f4
                                B0 = (rotate((((((BackupA0 + f4(BackupB0, BackupC0, BackupD0))) + get_part_message(messagePreparated, i, m)) + K[m + 48])), s[m + 48]) + BackupB0);
                                break;
                        }
                        if (j == 1 || j == 2 || j == 16 || j == 32)
                        {
                            Console.WriteLine("-------Конец итерации----------");
                            Console.WriteLine("A0 = {0} (16) = {1} (10)", A0.ToString("X"), A0);
                            Console.WriteLine("B0 = {0} (16) = {1} (10)", B0.ToString("X"), B0);
                            Console.WriteLine("C0 = {0} (16) = {1} (10)", C0.ToString("X"), C0);
                            Console.WriteLine("D0 = {0} (16) = {1} (10)", D0.ToString("X"), D0);
                            Console.WriteLine("------------------------------- \n\n");

                        }
                    }
                    if (k == 3)
                    {
                        Console.WriteLine("После перестановки");

                        Console.WriteLine("значение A = {0}", ((a0 % Int32.MaxValue) * (A0 % Int32.MaxValue)).ToString("X"));
                        Console.WriteLine("значение B = {0}", ((b0 % Int32.MaxValue) * (B0 % Int32.MaxValue)).ToString("X"));
                        Console.WriteLine("значение C = {0}", ((c0 % Int32.MaxValue) * (C0 % Int32.MaxValue)).ToString("X"));
                        Console.WriteLine("значение D = {0}", ((d0 % Int32.MaxValue) * (D0 % Int32.MaxValue)).ToString("X"));
                        Console.WriteLine("------------------------------- \n\n");
                    }
                    
                }



               


                
            }

            string hash = A0.ToString("X2") + B0.ToString("X2") + C0.ToString("X2") + D0.ToString("X2");
            return hash;
        }



        private static UInt32 rotate(UInt32 block, int value)
        {
            BitArray rotates = new BitArray(32);
            BitArray new_block = new BitArray(BitConverter.GetBytes(block));

            for (int i = 0; i < value; i++)
            {
                rotates[i] = new_block[i];
            }

            for (int i = value; i < 32; i++)
            {
                new_block[i - value] = new_block[value];
            }

            for (int i = 32 - value, j = 0; i < 32; i++, j++)
            {
                new_block[i] = rotates[j];
            }

            byte[] return_value = new byte[new_block.Length / 8];
            new_block.CopyTo(return_value, 0);
            return BitConverter.ToUInt32(return_value, 0);
        }

        private static UInt32 get_part_message(BitArray[][] message, int index_1, int index_2)
        {
            byte[] new_message = new byte[message[index_1][index_2].Length / 8];

            message[index_1][index_2].CopyTo(new_message, 0);
            return BitConverter.ToUInt32(new_message, 0);
        }

        //Раундовые функции RF
        private static UInt32 f1(UInt32 X, UInt32 Y, UInt32 Z)
        {
           return (X & Y) | (~X & Z);

        }

        //private static UInt32 f2(UInt32 X, UInt32 Y, UInt32 Z)
        //{
        //    return (X & Z) | (X & ~Z);
        //}

        private static UInt32 f2(UInt32 X, UInt32 Y, UInt32 Z)
        {
            return (X & Z) | (~Z & Y);
        }

        private static UInt32 f3(UInt32 X, UInt32 Y, UInt32 Z)
        {
            return X ^ Y ^ Z;
        }

        private static UInt32 f4(UInt32 X, UInt32 Y, UInt32 Z)
        {
            return Y ^ (~Z | X);
        }

        private static BitArray[][] Preparation(string message) //вовращает блоки по 512бит 16*32 бит
        {
            //--------реверс сообщения-------------------------
            char[] CompleteMessageCharArray = message.ToCharArray();
            Array.Reverse(CompleteMessageCharArray); //переворачиваем Chars
            message = new string(CompleteMessageCharArray);

            //--------создаем BitArray--------------
            BitArray completeBits = new BitArray(encoding.GetBytes(message));
            //-------шаг 1: добавление логической "1"-----------
            BitArray backup = completeBits;
            completeBits = new BitArray(backup.Length + 1);
            for (int i = 0; i < backup.Length; i++)
            {
                completeBits[i + 1] = backup[i];
            }
            completeBits[0] = true;
            //---------заполнение нулями до 512, если он уже равен 512, добавляется целый круг--------
            backup = completeBits;
            int NumberOf512s = completeBits.Length / 512;//количество блоков по 512 бит
            int NumberOfNulls = (NumberOf512s + 1) * 512 - completeBits.Length;//клдичество нулей, которое необходимо добавить в блок
            completeBits = new BitArray(backup.Length + NumberOfNulls);
            for (int i = 0; i < completeBits.Length; i++)
            {
                if (i < NumberOfNulls)
                {
                    completeBits[i] = false;
                }
                else
                {
                    completeBits[i] = backup[i - NumberOfNulls];
                }
            }

            //---------Длина исходного сообщения вложения (последние 'будут перезаписаны 0')---------------
            UInt64 backupLengthInt = (ulong)backup.Length - 1; //1 уже был вложен
            byte[] backupLengthByte = BitConverter.GetBytes(backupLengthInt);
            BitArray backupLength = new BitArray(backupLengthByte);
            if (backupLength.Length <= 64) //длинна сообщения должна иметь 64бита
            {
                for (int i = 0; i < backupLength.Length; i++)
                {
                    completeBits[i] = backupLength[i];
                }
            }
            else return null;
            //--------все части по 32 бита-------
            if (completeBits.Length % 512 == 0)
            {
                int NumberOf32s = completeBits.Length / 32;
                BitArray[] bloecke32 = new BitArray[NumberOf32s];
                for (int i = 0; i < NumberOf32s; i++)
                {
                    BitArray block32 = new BitArray(32);
                    for (int k = 0; k < 32; k++)
                    {
                        block32[k] = completeBits[k + i * 32];
                    
                    }
                    bloecke32[i] = block32;
                }
                //-------количество блоков по 512 бит и вставка количества в элемент---------
                NumberOf512s = completeBits.Length / 512;
                BitArray[][] bloecke512 = new BitArray[NumberOf512s][];
                for (int i = 0; i < NumberOf512s; i++)
                {
                    BitArray[] block512 = new BitArray[16];
                    for (int k = 0; k < 16; k++)
                    {
                        block512[k] = bloecke32[k + i * 16];
                    }
                    bloecke512[i] = block512;
                }
                return bloecke512;
            }
            else return null;
        }
    }
}
