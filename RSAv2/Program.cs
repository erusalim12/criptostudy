﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace RSAv2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            flag:
            Console.WriteLine("Введите текст для шифрования");
            var inputString = Console.ReadLine();

            Rsa(inputString.ToCharArray());
            Console.ReadLine();
           goto flag;

        }



        private static void Rsa(char[] text4Cripher)
        {
            var p = 127;//number 1
            var q = 13;//number 2
            var n = p*q;
            var fn = (p - 1)*(q - 1);//eiler Function

            var E =  OpenKey(n, fn);//open key

            if (NOD(E, fn) == 1) Console.WriteLine("Открытый ключ - валидный");//проверка ключа
            

            int d = ClosedKey(E, fn);

            if (NOD(E,d) == 1)Console.WriteLine("закрытый ключ - валидный");//проверка ключа 

            Console.WriteLine(text4Cripher);

            var str = Encrypt(text4Cripher, E, n);

            Console.WriteLine(str);
            Console.WriteLine(Decrypt(str, d, n));

            Console.WriteLine();

        }

        private static int ClosedKey(int e, int fn )
        {
            for(var i = 0;i<fn;i++)
            {
                if ((i*e)%fn == 1)
                {
                    return i;
                }
            }
            return 1;
        }


        private static char[] Encrypt(char[] text,int openKey,int n)
        {
            var mas = new char[text.Length];
            int i = 0;
            foreach (var c in text)
            {
                mas[i] = (char)MultipleByMod(value: c-1040, rate: openKey, mod: n);
                i++;
            }
          return mas;
        }
        private static char[] Decrypt(char[] text, int closedKey, int n)
        {
            var mas = new char[text.Length];
            int i = 0;
            foreach (var c in text)
            {
                mas[i] = (char)MultipleByMod(value: c+1040, rate: closedKey, mod: n);
                i++;
            }
            return mas;
        }

        private static int MultipleByMod(int value, int rate, int mod)
        {
            var result = 1;
            while (rate > 0)
            {
                if (rate%2==0)
                {
                    rate /= 2;
                    value = value*value%mod;
                }
                else
                {
                    rate--;
                    result = (result*value)%mod;
                }
            }
            return result;

        }

     
        #region НОД
        /// <summary>
        /// Возвращает наименьший общий делитель       
        /// </summary>
        /// <param name="a">число 1</param>
        /// <param name="b">число 2</param>
        /// <returns></returns>
        private static int NOD(int a, int b)
        {
            if (a == b)
                return a;
            else if (a > b)
                return NOD(a - b, b);
            else
                return NOD(b - a, a);
        }
        #endregion

        #region генерация открытого ключа
        /// <summary>
        /// возвращает открытый ключ
        /// </summary>
        /// <param name="maxValue">Максимальное значение открытого ключа </param>
        /// <param name="eilerValue">Возвращает значение функции Эйлера</param>
        /// <returns></returns>
        private static int OpenKey(int maxValue, int eilerValue)
        {
            Random rand = new Random(DateTime.Now.Millisecond);
            int openKey = 0;
            bool IsFoundedValue = false;

            while (!IsFoundedValue)
            {
                openKey = rand.Next(maxValue - 1);
                if (NOD(openKey, eilerValue) == 1)
                {
                    IsFoundedValue = true;
                }
            }
            return openKey;

        }
        #endregion
    }
}
