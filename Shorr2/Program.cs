﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Shorr2
{
    class Program
    {
        static void Main(string[] args)
        {
            var alice = new Alice(23, 11);
            var bobby = new Bob();


            var r = Alice.R; //отправить бобу
            var e = Bob.E; //отправить алисе
            var s = alice.CalculateS(e); //отправить бобу
            alice.Compare(r, e, s);

            Console.ReadLine();
        }

        public static int MultipleByMod(int value, int rate, int mod)
        {
            var result = 1;
            while (rate > 0)
            {
                if (rate%2 == 0)
                {
                    rate /= 2;
                    value = value*value%mod;
                }
                else
                {
                    rate--;
                    result = (result*value)%mod;
                }
            }
            return result;

        }

    }

    public class Alice
    {
        public Alice(int p, int q)
        {
            P = p;
            Q = q;
            X = new Random().Next(q - 1);
            Console.WriteLine("X val =" + X);
            G = CalculateG(p, q);
            Y = CalculateY(G, X, P);
            K = new Random().Next(q - 1);
            Console.WriteLine("K val =" + K);

            R = CalculateR(K, G, P);


        }
        public bool Compare(int R,int E,int s)
        {

            var G1 = Program.MultipleByMod(G, s, P);
            var Y1 = Program.MultipleByMod(Y, E, P);
            Console.WriteLine("Сравниваем R и G^s mod p * Y^e mod p ");
            Console.WriteLine("R={0}, G^s={1},Y^e={2}", R, G1, Y1);
            if (R == (G1*Y1)%P)
            {
                
                Console.WriteLine("авторизация успешна");
                return true;
            }
            Console.WriteLine("Попробуйте ещё разок");

            return false;
        }
        private static int P { get; set; }
        private static int Q { get; set; }
        private static int X { get; set; }
        private static int G { get; set; }
        public static int Y { get; set; }
        public static int K { get; set; }
        public static int S { get; set; }
        public static int R { get; set; }

        private int CalculateX(int p, int q)
        {
            int x = p*q;
            Console.WriteLine("Алиса выбирает X = {0}", x);
            return x;
        }

        private int CalculateG(int p, int q)
        {

            for (int i = 2; i < Q; i++)
            {
                int Gcandidat = Program.MultipleByMod(i, q, p);
                if (Gcandidat == 1)
                {
                    G = i;
                    Console.WriteLine("Алиса выбирает G={0}", G);
                    return i;
                }
            }
            return -1;
        }

        private int CalculateY(int g, int x, int p)
        {
            int v = 0;
            for (int i = 2; i < Q; i++)
            {
                int yCand = (Program.MultipleByMod(g, x, p)*i%p)%p;
                if (yCand == 1)
                {
                    Y = i;
                    v = i;
                    Console.WriteLine("Алиса выбирает Y= {0}", Y);
                }
            }
            return v;
        }

        private int CalculateR(int k, int g, int p)
        {
            R = Program.MultipleByMod(g, k, p);
            Console.WriteLine("Алиса выбирает R= {0}", R);
            return R;
        }

        public int CalculateS(int e)
        {
            int s = (K + X*e)%Q;
            Console.WriteLine("Алиса выбирает S= {0}", s);
            S = s;
            return s;
        }
    }

    public class Bob
    {
        public Bob()
        {
            E = MakeE();
        }

        public static int E { get; set; }

        public static int T = 3;


        private int MakeE()
        {
            E = new Random().Next((int) Math.Pow(2, T));
            Console.WriteLine("БОБ вычисляет E={0}, T={1}", E,T);
            return E;
        }

       
    }
}

