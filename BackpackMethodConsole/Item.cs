﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackpackMethodConsole
{
    class Item
    {
        public int Name { get; set; }
        public double Cost { get; set; }
        public double Weight { get; set; }
    }
}
