﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace FeigeFiatShamir
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Укажите количество раз аккредитаци");
            int T;
            f:
            try
            {
                T = int.Parse(Console.ReadLine());

            }
            catch (Exception)
            {
                Console.WriteLine("Введите число");
                goto f;
            }
            List<bool>returnFrq = new List<bool>();
            for (int i = 0; i < T; i++)
            {
                //int p = new bulkPrimes().getPrime(30,50);
                //int q = new bulkPrimes().getPrime(2,30);
                //var sammy = new Sammy(p,q); 

                var sammy = new Sammy(13,113);
                var alice = new Alice(sammy.N,sammy.S);
                var bob = new Bob(sammy.N,sammy.V);

                returnFrq.Add(bob.Calculate(alice.sendToBobRorY(bob.SentRandomBit())));
                
            }
            Console.WriteLine("Вероятность идентификации составляет:{0} из {1}", returnFrq.Count(item => item),T);

            
            goto f;
            Console.WriteLine("\n\n\n\nЕщё разок?(y/n)?");
            var text = Console.Read();
            if (text == 'y'||text=='Y')
            {
                goto f;
            }
        }

        public static int MultipleByMod(int value, int rate, int mod)
        {
            var result = 1;
            while (rate > 0)
            {
                if (rate % 2 == 0)
                {
                    rate /= 2;
                    value = value * value % mod;
                }
                else
                {
                    rate--;
                    result = (result * value) % mod;
                }
            }
            return result;

        }

        /// <summary>
        /// Возвращает наименьший общий делитель       
        /// </summary>
        /// <param name="a">число 1</param>
        /// <param name="b">число 2</param>
        /// <returns></returns>
        public static int NOD(int a, int b)
        {
            if (a == b)
                return a;
            else if (a > b)
                return NOD(a - b, b);
            else
                return NOD(b - a, a);
        }
    }

    public class Alice//A
    {
        public Alice(int n,int s)
        {
            N = n;//mod
            S = s;//закрытый ключ, переданный арбитром
            R = GetZ(n);
        }

        private int S;
        public int R;
        private int N;
        private int Z;
        private int GetZ(int n)
        {
            var rand = new Random().Next(n);
            Console.WriteLine("Алиса выбирает число (1-(n-1))={1}; n={0}",n,rand);

            var z = Program.MultipleByMod(rand, 2, n);
            Console.WriteLine("Алиса вычисляет Z={0}",z);
            Z = z;
            Console.WriteLine("И отправляет Бобу");
            return z;
        }

        public int sendToBobRorY(byte bit)
        {
          
            if (bit == 1)
            {
                int Y = Program.MultipleByMod(R, S, N);
                Console.WriteLine("Aлиса отправляе бобу Y ={0}",Y);
                return Y;
            }
            Console.WriteLine("Aлиса отправляе бобу R ={0}", R);
            return R;
        }
    }

    public class Bob//B
    {
        public Bob(int n,int v)
        {
            N = n;//открытые ключи
            V = v;
        }

        private byte bit;
        private int N;
        private int V;
        public byte SentRandomBit()
        {
            var rand = new Random();
            if (rand.Next()%2 == 0)
            {
                bit = 1;
                Console.WriteLine("боб послал Алисе \"1\"");
                return 1;
            }
            bit = 0;
            Console.WriteLine("боб послал Алисе \"0\"");
            return 0;
        }

        public bool Calculate(int value)
        {
            Console.WriteLine("Боб осуществляет проверку результатов");
            int z;
            if (bit == 1)
            {

                z = Program.MultipleByMod(value, 2, N) * V % N;
                
            }
            else
            {
                z = Program.MultipleByMod(value, 2, N);
            }
            Console.WriteLine("Z = {0}, value = {1}",z,value);
            Console.WriteLine(z == value);
            return z == value;
        }
    }

    public class Sammy //arbiter
    {
        public Sammy(int p = 5, int q = 7)
        {
            this.p = p;
            this.q = q;
            N = p*q;//вычисляем N
            Console.WriteLine("Сэмии вычисляет n={0} из полученных p={1},q={2}", N, p, q);

            CreateV(N);
            CalculateS(N,V1);
        }

        private int p { get; set; }
        private int q { get; set; }
        public int N { get; set; }

        public int V { get; set; }
        public int V1 { get; set; }
        public int S { get; set; }

/// <summary>
/// тута и злеся
/// </summary>
/// <param name="mod"></param>
        private void CreateV(int mod)
        {
            List<int> candidatesV = new List<int>();
            List<int> candidatesV1 = new List<int>();

            for (int x = 0; x < mod; x++)
            {
                var cand = (x * x) % mod;
                var cand1 = ReverseValue(cand, mod);
                if (cand != cand1) continue;
                candidatesV.Add(cand);
                candidatesV1.Add(cand1);
            }
            int rand = new Random().Next(candidatesV.Count - 1);
            V = candidatesV[rand];
            V1 = candidatesV1[rand];
            Console.WriteLine("Сэмми вычисляет V = {0}, и V^-1 = {1}", V, V1);
            Console.WriteLine("n = {0} ", mod);
        }

        private void candidateV2(int mod)
        {
            List<int> candidatesV = new List<int>();
            for (int x = 1; x < mod; x++)
            {
                if ((x*x)%mod ==)
                {
                    
                }
                
            }            
        }

        //возвращает E^-1, если оно существует
        //или -1, если не существует
        private int ReverseValue(int e, int mod)
        {
            for (var i = 0; i < mod; i++)
            {
                if ((i * e) % mod == 1)
                {
                    return i;
                }
            }
            return -1;
        }

        void CalculateS(int n, int v1)
        {
            for (int i = n; i >0; i--)
            {
                int sCand = Program.MultipleByMod(i, 2, n);
                if (sCand != 1 && sCand == v1)
                {
                    S = i;
                }
            }
            Console.WriteLine("Сэмми вычисляет S={0}",S);
        }
    }
}