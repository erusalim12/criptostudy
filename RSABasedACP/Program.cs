﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RSABasedACP
{
    class Program
    {
        
        static void Main(string[] args)
        {
            f:
            Console.WriteLine("Введите текст:");
            string text = Console.ReadLine();
            string hash = MD5DotNet(text);

            Console.WriteLine("Результат хеш-функции");
            Console.WriteLine(hash);

            KeyGenerator kg = new KeyGenerator(11,107);
            var alice = new Alice(text,kg.D,kg.N);

            var bob = new Bob(text,alice.DigitalSignature,kg.E,kg.N);

            goto f;
        }

        public static string MD5DotNet(string input)
        {

            // step 1, calculate MD5 hash from input

            MD5 md5 =MD5.Create();

            byte[] inputBytes = Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);


            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {

                sb.Append(hash[i].ToString("X2"));

            }

            return sb.ToString();

        }



        public static string RecievedMessageHash(string text)
        {
            return  MD5DotNet(text);
        }

        public static int MultipleByMod(int value, int rate, int mod)
        {
            var result = 1;
            while (rate > 0)
            {
                if (rate % 2 == 0)
                {
                    rate /= 2;
                    value = value * value % mod;
                }
                else
                {
                    rate--;
                    result = (result * value) % mod;
                }
            }
            return result;

        }
    }


    public class Alice
    {
        public Alice(string text,int d,int mod)
        {
            Text = text;
            Hash = Program.MD5DotNet(text);
            DigitalSignature = GetDigitalSignature(Hash, d, mod);
        }
        public string Text { get; set; }

        public string Hash { get; set; }

        public int[] DigitalSignature { get; set; }

        private int[] GetDigitalSignature(string inputText,int closedKey,int mod)
        {
            byte[] inputBytes = Encoding.ASCII.GetBytes(inputText);

            List<int> code = inputBytes.Select(t => Program.MultipleByMod(t, closedKey, mod)).ToList();

            return code.ToArray();
        }

    }

    public class Bob
    {
        public Bob(string text,int[] hash,int e,int n)
        {
            N = n;
            E = e;
            Text = text;
            hash1 = DigitalHash(hash, e, n);
            Compare(hash, hash1);

        }

        public int N;
        private int E;
        private string Text;
        private int[] hash1;
        public static string RecievedMessageHash(string text)
        {
            return Program.MD5DotNet(text);
        }

        public int[]  DigitalHash(int[] key, int e,int n)
        {
            return key.Select(item => Program.MultipleByMod(item, e, n)).ToArray();
        }
        
        public bool Compare (int[] hash1, int[] hash2)
        {
            Console.WriteLine(intToString(hash1));
            Console.WriteLine(intToString(hash2));
            if (hash1.SequenceEqual(hash2))
            {
               Console.WriteLine("Валидно");
                return true;
            }
            Console.WriteLine("инВалидно");
            return false;

        }

        private string intToString(int[] arr)
        {
            return arr.Aggregate<int, string>(null, (current, i) => current + i);
        }
    }


   public class KeyGenerator
    {
        public KeyGenerator( int p = 29, int q = 107)
        {
          
            P = p;
            Q = q;
            N = p*q;
            Fn = (P - 1)*(Q - 1);
            E = OpenKey(N, Fn);
            D = ClosedKey(E, Fn);

        }
        public int Fn;
        public int E;
        public int N;

        public int D;


        private int P;
        private int Q;

        public static int NOD(int a, int b)
        {
            if (a == b)
                return a;
            else if (a > b)
                return NOD(a - b, b);
            else
                return NOD(b - a, a);
        }

        private int OpenKey(int maxValue, int eilerValue)
        {
            Random rand = new Random(DateTime.Now.Millisecond);
            int openKey = 0;
            bool IsFoundedValue = false;

            while (!IsFoundedValue)
            {
                openKey = rand.Next(maxValue - 1);
                if (NOD(openKey, eilerValue) == 1)
                {
                    IsFoundedValue = true;
                }
            }
            return openKey;

        }

        private int ClosedKey(int e, int fn)
        {
            for (var i = 0; i < fn; i++)
            {
                if ((i * e) % fn == 1)
                {
                    return i;
                }
            }
            return 1;
        }
    }
}
