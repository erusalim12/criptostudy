﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSA
{
    public class Prime
    {

        public bool ifPrime(int prime)
        {
            if (prime < 2)
                return false;

            for (int i = 2; i * i <= prime; i++)
            {
                if (prime % i == 0)
                    return false;
            }
            return true;
        }
    }

    public class bulkPrimes
    {
        Prime pri = new Prime();

        private int Primes(int min = 0, int max = 1000)
        {
            Random rand = new Random();
            while (true)
            {
                var i = rand.Next(min, max);
                if (pri.ifPrime(i))
                {
                    return i;
                }
            }
        }
        public int getPrime(int min = 0, int max = 1000)
        {

            return Primes(min, max);
        }
    }
   
}
