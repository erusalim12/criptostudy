﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSA_Auth
{
    class Program
    {
        static void Main(string[] args)
        {
            var alice = new Alice();
            var bobby = new Bob();

            //алиса генерирует открытый и закрытый ключи
          //  var aliceOpenKeys = alice.SendRequestToBob();//+
            //боб получает ключ от алисы
            //боб выбирает случайное число к, вычисляет и посылает алисе  r = k pow(e) mod n и посылает r алисе.
           // int RToAlice = bobby.GetKeysFromAlice(alice.SendRequestToBob());
            //алиса посылает бобу к'
       //     int KtoBobby = alice.SolveK(bobby.GetKeysFromAlice(alice.SendRequestToBob()));
            //Боб сравнивает ключи

            bobby.BobbyAnswer(alice.SolveK(bobby.GetKeysFromAlice(alice.SendRequestToBob())));

            Console.ReadLine();

        }
        public static int MultipleByMod(int value, int rate, int mod)
        {
            var result = 1;
            while (rate > 0)
            {
                if (rate % 2 == 0)
                {
                    rate /= 2;
                    value = value * value % mod;
                }
                else
                {
                    rate--;
                    result = (result * value) % mod;
                }
            }
            return result;
        }
    }

    public class Alice
    {
        private int e { get; set; } //e
        private int d { get; set; } //d
        private int n { get; set; } //n
        private int k1 { get; set; } //k'

        public OpenKey SendRequestToBob(int p = 127, int q = 31)
        {
            n = p*q;
            int fn = (p - 1)*(q - 1);

            e = OpenKey(n, fn);
            d = ClosedKey(e, fn);

            Console.WriteLine("@алиса генерирует открытый и закрытый ключи и передает открытый Бобу@");
            Console.WriteLine("Открытый ключ: \n E={0} \n n={1}", e, n);
            Console.WriteLine("Закрытый ключ:  d={0}", d);

            return new OpenKey(e, n);
        }

        private int ClosedKey(int e, int fn)
        {
            for (var i = 0; i < fn; i++)
            {
                if ((i*e)%fn == 1)
                {
                    return i;
                }
            }
            return 1;
        }

        private int OpenKey(int maxValue, int eilerValue)
        {
            Random rand = new Random(DateTime.Now.Millisecond);
            int openKey = 0;
            bool IsFoundedValue = false;

            while (!IsFoundedValue)
            {
                openKey = rand.Next(maxValue - 1);
                if (NOD(openKey, eilerValue) == 1)
                {
                    IsFoundedValue = true;
                }
            }
            return openKey;
        }


        private int NOD(int a, int b)
        {
            if (a == b)
                return a;
            else if (a > b)
                return NOD(a - b, b);
            else
                return NOD(b - a, a);
        }


        public int SolveK(int r)
        {
            k1 = Program.MultipleByMod(r, d, n);
            return k1;
        }
    }
    public class Bob
    {
        public OpenKey AliceSendsKey { get; set; }
        private int K { get; set; }

        //Вычисляем и отправляем R к алисе
        //боб выбирает случайное число к, вычисляет и посылает алисе  r = k pow(e) mod n и посылает r алисе.

        public int GetKeysFromAlice(OpenKey aliceKey)
        {
            AliceSendsKey = aliceKey;
            K = new Random().Next(aliceKey.N - 1);
            int r = Program.MultipleByMod(K, aliceKey.E, aliceKey.N);
            Console.WriteLine("@боб выбирает случайное число к, вычисляет и посылает алисе  r = k pow(e) mod n и посылает r алисе@");
            Console.WriteLine(" k={0}, r={1}",K,r);

            return r;
        }

        //k- ключ боба
        //k1- ключ Алисы
        private static bool CompareK(int k, int k1)
        {
            return k == k1;
        }

        public void BobbyAnswer(int k1)
        {

            Console.WriteLine("Боб сравнивает свой к= {0}, с ответом Алисы= {1}\n",K,k1);

            Console.WriteLine(CompareK(K, k1) ? "Приветике!" : "Вы кто такие? Я вас не звал! идите...");
        }
    }
    public class OpenKey
    {
        public OpenKey(int e, int n)
        {
            E = e;
            N = n;
        }

        public int E { get; set; }
        public int N { get; set; }
    }
}
