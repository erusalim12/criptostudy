﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BackPack2
{
    static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите текст");
            var openText = Console.ReadLine();
            if (openText != null)
            {
                string binaryText = BinaryToString(openText);                
            }


        }


        private int SuperGrowing(int value)
        {
            
        }

        //строка в биты
        static string StringToBinary(string data)
        {
            StringBuilder sb = new StringBuilder();

            foreach (char c in data)
            {
                sb.Append(Convert.ToString(c, 2).PadLeft(8, '0'));
            }
            return sb.ToString();
        }
        //биты в строку
        public static string BinaryToString(string data)
        {
            List<Byte> byteList = new List<Byte>();

            for (int i = 0; i < data.Length; i += 8)
            {
                byteList.Add(Convert.ToByte(data.Substring(i, 8), 2));
            }
            return Encoding.ASCII.GetString(byteList.ToArray());
        }

    }
}
