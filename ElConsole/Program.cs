﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace ElConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            A:
           // Console.OutputEncoding = Encoding.Unicode;
            int p = 1009;
            int g = GetProot(p);
            int x = new Random().Next(2,p);
            int y = MultipleByMod(g, x, p);
            Console.Write("Введите текст: \n");
            char[] text = Console.ReadLine().ToCharArray();
            char[] crypted = Crypt(text, y, g, p);
            char[] decrypted = Decrypt(new string(crypted), x, p);
            Console.WriteLine();

            
            Console.WriteLine("Введеный текст: {0}", new string(text));
            Console.WriteLine("Шифротекст: {0}", new string(crypted));
            Console.WriteLine("Дешифрованный текст: {0}", new string(decrypted));
      

            goto A;
        }
      static  char[] Alphabet ={ '#', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И',
                                                        'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С',
                                                        'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ь', 'Ы', 'Ъ',
                                                        'Э', 'Ю', 'Я', ' ', '1', '2', '3', '4', '5', '6', '7',
                                                        '8', '9', '0' };


        private static char[] Crypt(char[] text, int y, int g, int p)
        {
            int k;
            var rand = new Random(DateTime.Now.Millisecond);
            
            List<char>crypted = new List<char>();
            foreach (char T in text)
            {
                int index = Array.IndexOf(Alphabet, T);
                k = rand.Next(2, p - 1);
                crypted.Add((char) MultipleByMod(g, k, p));
                crypted.Add((char)(( MultipleByMod(y, k, p)* index) %p));
            }
            return crypted.ToArray();
        }

        private static char[] Decrypt(string text, int x, int p)
        {
            string T = null;
            List<char>symbols = new List<char>();
           
            for (int i = 0; i < text.Length; i += 2)
            {
                int a = text[i];
                int b = text[i + 1];

                // m=b*(a^x)^(-1)mod p =b*a^(p-1-x)mod p - трудно было  найти нормальную формулу, в ней вся загвоздк
                int index = ((b*MultipleByMod(a, p - 1 - x, p))%p);
                symbols.Add(Alphabet[index]);
             
            }
            return symbols.ToArray();
        }


        private static int MultipleByMod(int value, int rate, int mod)
        {
            var result = 1;
            while (rate > 0)
            {
                if (rate % 2 == 0)
                {
                    rate /= 2;
                    value = value * value % mod;
                }
                else
                {
                    rate--;
                    result = (result * value) % mod;
                }
            }
            return result;

        }
        private static int GetProot(int p)
        {
            for (int i = 0; i < p; i++)
            {
                if (IsRoot(p, i))
                {
                    return i;
                }
            }
            return 0;
        }

        private static bool IsRoot(int p, int a)
        {
            if (a == 0 || a == 1) return false;
            int last = 1;
            HashSet<int> set = new HashSet<int>();
            for (int i = 0; i < p - 1; i++)
            {
                last = (last * a) % p;
                if (set.Contains(last)) return false;
                set.Add(last);
            }
            return true;
        }

    }
}
