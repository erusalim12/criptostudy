﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RSA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            textBox1.Text = new string(Alphabet);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var text = textBox1.Text.ToCharArray();
            Rsa(text);

        }

        private void Rsa(char[] text4Cripher)
        {
            var p = 127; //number 1 
            var q = 13; //number 2
            var n = p*q;
            var fn = (p - 1)*(q - 1); //eiler Function

            var E = OpenKey(n, fn); //open key

            int d = ClosedKey(E, fn);

           
            var encryptedText = Encrypt(text4Cripher, E, n);
            var str1 = String.Concat(encryptedText);
            textBox2.Text = str1; //new string(String.Join(encryptedText),Encoding.Unicode);

            var str2 = String.Concat(Decrypt(encryptedText,d,n));
            textBox3.Text = str2;

        }

        private  int ClosedKey(int e, int fn)
        {
            for (var i = 0; i < fn; i++)
            {
                if ((i*e)%fn == 1)
                {
                    return i;
                }
            }
            return 1;
        }


        private  char[] Encrypt(char[] text, int openKey, int n)
        {
            var mas = new char[text.Length];
            int i = 0;
           
            foreach (var c in text)//проход по строке
            {
                int index = Array.IndexOf(Alphabet, c);
                mas[i] = (char)MultipleByMod(value: index, rate: openKey, mod: n);
                i++;
            }
            return mas;
        }

        private  char[] Decrypt(char[] text, int closedKey, int n)
        {
            var mas = new char[text.Length];
            int i = 0;
            foreach (var c in text)
            {
                mas[i] = Alphabet[MultipleByMod(value: c, rate: closedKey, mod: n)];
                i++;
                
            }
            return mas;
        }

        private  int MultipleByMod(int value, int rate, int mod)
        {
            var result = 1;
            while (rate > 0)
            {
                if (rate%2 == 0)
                {
                    rate /= 2;
                    value = value*value%mod;
                }
                else
                {
                    rate--;
                    result = (result*value)%mod;
                }
            }
            return result;

        }

        char[] Alphabet ={ '#', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И',
                                                        'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 
                                                        'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ь', 'Ы', 'Ъ',
                                                        'Э', 'Ю', 'Я', ' ', '1', '2', '3', '4', '5', '6', '7',
                                                        '8', '9', '0' };
        

        #region НОД

        /// <summary>
        /// Возвращает наименьший общий делитель       
        /// </summary>
        /// <param name="a">число 1</param>
        /// <param name="b">число 2</param>
        /// <returns></returns>
        private  int NOD(int a, int b)
        {
            if (a == b)
                return a;
            else if (a > b)
                return NOD(a - b, b);
            else
                return NOD(b - a, a);
        }

        #endregion

        #region генерация открытого ключа

        /// <summary>
        /// возвращает открытый ключ
        /// </summary>
        /// <param name="maxValue">Максимальное значение открытого ключа </param>
        /// <param name="eilerValue">Возвращает значение функции Эйлера</param>
        /// <returns></returns>
        private  int OpenKey(int maxValue, int eilerValue)
        {
            Random rand = new Random(DateTime.Now.Millisecond);
            int openKey = 0;
            bool IsFoundedValue = false;

            while (!IsFoundedValue)
            {
                openKey = rand.Next(maxValue - 1);
                if (NOD(openKey, eilerValue) == 1)
                {
                    IsFoundedValue = true;
                }
            }
            return openKey;

        }

        #endregion
    }

}
