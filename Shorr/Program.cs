﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shorr
{
    class Program
    {
        static void Main(string[] args)
        {
            var alice = new Alice();
            var bobby = new Bob();

            //алиса выбирает к, вычисляет R и посылает бобу 

            bobby.SetR(alice.SendR());
            int bobE = bobby.MakeE();
            //алиса вычисляет S и посылает бобу
            int aliseS = alice.MakeS(bobE);
            bobby.SetS(aliseS);
            bobby.Result(alice.G, alice.Y, alice.P);


            Console.Read();
        }

        public static int MultipleByMod(int value, int rate, int mod)
        {
            var result = 1;
            while (rate > 0)
            {
                if (rate%2 == 0)
                {
                    rate /= 2;
                    value = value*value%mod;
                }
                else
                {
                    rate--;
                    result = (result*value)%mod;
                }
            }
            return result;

        }
    }


    class Alice
    {
        public Alice(int p = 97, int q = 11)
        {
            Console.WriteLine("Алисв");
            P = p;
            Q = q;
        }

        public int SendR()
        {
            K = new Random().Next(Q - 1);
            R = Program.MultipleByMod(G, K, P);

            return R;
        }

        public int MakeS(int e)
        {
            return (K + X*e)%Q;
        }

        public int P; //два простых числа
        public int Q;

        public int X; //+
        public int G; //+
        public int Y; //+

        public int K;
        public int R;



        #region вычисление ключей

        private int selectQ(int p)
        {
            for (int i = p; i == 0; i--)
            {
                if ((p - 1)%i == 0)
                {
                    Q = i;
                    setXval(Q);
                    return i;
                }
            }
            return 1;
        }


        private void setXval(int q)
        {
            X = new Random().Next(q - 1);
        }

        private void setGval(int p, int q)
        {
            for (int i = 0; i < Q; i++)
            {
                if (Program.MultipleByMod(i, q, p) == 1)
                {
                    G = i;
                }
            }

        }

        private void setYVAl(int g, int x, int p)
        {
            for (int i = 0; i < Q; i++)
            {
                if ((Program.MultipleByMod(g, x, p)*i)%p == 1)
                {
                    Y = i;
                }
            }
        }



        #endregion
    }



    class Bob
    {
        public int E;
        public int T = 3;
        public int R;
        public int S;

        public void SetS(int val)
        {
            S = val;
        }

        public void SetR(int val)
        {
            R = val;
        }

        public int MakeE()
        {
            E = new Random().Next((int) Math.Pow(2, T));
            return E;
        }

        public bool Compare(int g, int y, int p)
        {
            var G1 = Program.MultipleByMod(g, S, p);
            var Y1 = Program.MultipleByMod(y, E, p);
            return R == (G1*Y1)%p;


        }

        public void Result(int g, int y, int p)
        {
            Console.WriteLine(Compare(g, y, p) ? "приветекит!" : "Снова мима!");
        }
    }
}
